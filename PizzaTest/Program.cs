﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace PizzaTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var httpClient = new HttpClient())
            {
                Console.WriteLine("----- Downloading pizza data -----");
                var json = httpClient.GetStringAsync("http://files.olo.com/pizzas.json").Result;
                Console.WriteLine("----- Download complete -----");

                var pizzas = JsonConvert.DeserializeObject<List<Pizza>>(json); 
                var combos = new List<string>();
                var groupedCombos = new List<Combo>();

                foreach (var item in pizzas)
                {
                    combos.Add(string.Join(",", item.Toppings));
                }

                var grouped = combos.GroupBy(x => x)
                    .Select(g => new { Text = g.Key, Count = g.Count() });

                foreach (var item in grouped)
                {
                    groupedCombos.Add(new Combo { Toppings = item.Text, Count = item.Count });
                };

                var result = groupedCombos.OrderByDescending(x => x.Count).Take(20); // Getting the top 20 toppings

                // Displaying result
                Console.WriteLine($"");
                Console.WriteLine($"Rank | Toppings | Count");
                Console.WriteLine("-----------------------------------");
                foreach (var item in result.Select((value, i) => new { value, i }))
                {
                    Console.WriteLine($"{ item.i + 1 } | { item.value.Toppings } | { item.value.Count }");
                }

                Console.ReadLine();
            }
        }

        public class Pizza
        {
            public List<string> Toppings { get; set; }
        }

        public class Combo
        {
            public string Toppings { get; set; }
            public int Count { get; set; }
        }
    }
}
